#!/usr/bin/make
# vim: noet ft=make


.PHONY: all
all:

DESTDIR = /usr
SCRIPTS = $(notdir $(wildcard scripts/*))
BINS = $(notdir $(wildcard bin/*))
BIN_MANUALS = $(addsuffix .1,$(BINS))

MAN_DIR := $(DESTDIR)/share/man/man1

INSTALLED_SCRIPTS = $(addprefix $(DESTDIR)/share/pkg-perl-tools/,$(SCRIPTS))
INSTALLED_SCRIPT_MANUALS = $(add-prefix $(MAN_DIR)/dpt-,$(addsuffix .1,$(SCRIPTS)))
INSTALLED_BINS = $(addprefix $(DESTDIR)/bin/,$(BINS))
INSTALLED_BIN_MANUALS = $(addprefix $(MAN_DIR)/,$(BIN_MANUALS))

INSTALL := install -D -m 0644
INSTALL_BIN := install -D -m 0755

.PHONY: install
install: $(INSTALLED_SCRIPTS) $(INSTALLED_SCRIPT_MANUALS) \
    $(INSTALLED_BINS) $(INSTALLED_BIN_MANUALS)

$(MAN_DIR):
	$(INSTALL) -d -m 0755 $@

$(INSTALLED_SCRIPTS): $(DESTDIR)/share/pkg-perl-tools/% : scripts/%
	$(INSTALL_BIN) $< $@

$(INSTALLED_SCRIPT_MANUALS) : $(MAN_DIR)/dpt-%.1 : scripts/% $(MAN_DIR)
	pod2man $< > $@

$(INSTALLED_BINS) : $(DESTDIR)/bin/% : bin/%
	$(INSTALL_BIN) $< $@
	sed -i 's,SCRIPTS=scripts # REPLACED.*,SCRIPTS="$(DESTDIR)/share/pkg-perl-tools",' $@

$(INSTALLED_BIN_MANUALS) : $(MAN_DIR)/%.1 : bin/% $(MAN_DIR)
	pod2man $< > $@

.PHONY: clean
clean:

test:
	sh -n bin/dpt
