This is a collection of proof-of-concept scripts to tie
git-debcherry into a pkg-perl workflow, as prepared for
the Perl Team Sprint in Barcelona, May 2015.

Contents:

git-debcherry-nonumbered
    git-debcherry script from gitpkg_0.27, enhanced to not write commit
    numbers in the subject line

quilt2debcherry
    script to convert quilt patches in a package into a commit series
    with the patch metadata stored as git notes

run-debcherry
    script to automate exporting patches from the commit series and notes,
    by first running git-debcherry itself and then mangling the results
    aiming for stable output with the two scripts below

notes2dep3
    script to convert "git format-patch" output of commits with patch
    metadata inside git notes into a DEP3-compatible format

rename-patches
    script to rename patches as created by "git format-patch" into
    names spefied by the "Patch-Name" header stored in the git notes

20150524 /ntyni
