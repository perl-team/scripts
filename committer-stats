#!/bin/sh

# Copyright 2012-2024, gregor herrmann <gregoa@debian.org>
# Copyright 2014, Salvatore Bonaccorso <carnil@debian.org>
# Released under the same terms as Perl

# INSTRUCTIONS:
# * to be run at the top of the packages direcory
#   unless DPT_PACKAGES is set in ~/.dpt.conf

set -eu

skip() {
    echo >&2 "$1"
    cd ..
}

for f in $HOME/.dpt.conf $HOME/.config/dpt.conf; do
    if [ -r "$f" ]; then
        . "$f"
    fi
done

if [ -n "${DPT_PACKAGES:-}" ]; then
	TOP=${DPT_PACKAGES}
else
	TOP="."
fi


COMMITTERS=$(mktemp)
trap "rm -f $COMMITTERS" INT QUIT TERM EXIT

CURDIR=$(pwd)
cd "$TOP"

for PKG in $(ls -1d */ | sed -e 's:/$::') ; do
    cd $PKG     || continue
    dh_testdir  || { skip "$PKG doesn't look like a debian package."; continue; }
    [ -d .git ] || { skip "No .git directory in $PKG."; continue; }

    CURBRANCH=$(git branch --show-current)
    [ -n "$(git branch -r --list origin/HEAD)" ] || git remote set-head origin -a
    HBRANCH=$(git branch -r --list origin/HEAD --format '%(symref:lstrip=3)')
    [ "$CURBRANCH" = "$HBRANCH" ] || git checkout -q origin/$HBRANCH
    git log --pretty=tformat:"%cn" --since="1 year ago" -- debian/ >> $COMMITTERS
    [ "$CURBRANCH" = "$HBRANCH" ] || git checkout -q $CURBRANCH

    cd ..
done
cd "$CURDIR"

echo "Committers in pkg-perl packages (debian/) between $(date --date '1 year ago' +%F) and $(date +%F)"
echo "==========================================================================="
echo

perl -Mutf8::all -MUnicode::Collate -nlE '
	$c{$_}++;
	END {
		$uc  = Unicode::Collate->new();
		say "Count\n-----\n", scalar keys %c, "\n";
		say "By name\n-------";
		printf "%-35s %5d\n", $_, $c{$_} foreach sort { $uc->cmp($a, $b) } keys %c;
		say "\nBy commits\n----------";
		printf "%-35s %5d\n", $_, $c{$_} foreach reverse sort { $c{$a} <=> $c{$b} } keys %c;
	}
' "$COMMITTERS"
