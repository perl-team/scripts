#!/usr/bin/perl

# Copryright © 2014 by Axel Beckert <abe@debian.org>
#
# License:
#
#         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#                    Version 2, December 2004
#
# Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
#
# Everyone is permitted to copy and distribute verbatim or modified
# copies of this license document, and changing it is allowed as long
# as the name is changed.
#
#            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
#   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
#
#  0. You just DO WHAT THE FUCK YOU WANT TO.
#

use warnings;
use strict;

use FindBin qw($Bin);
use Parse::DebianChangelog;
use Getopt::Long qw(:config auto_help);

# Init
my @changelogs = <$Bin/../packages/*/debian/changelog>;
my %result = ();
my %date = ();
my %timestamp = ();
my %noupload = ();

# Defaults
my $bydate = 0;
my $bylength = 0;
my $never = -1;

# Getopt
GetOptions ('date' => \$bydate,
            'time' => \$bydate,
            'length' => \$bylength,
            'unreleased' => \$bylength,
            'never!' => \$never);
$bydate = 1 unless $bylength or $bydate;

foreach my $file (@changelogs) {
    my $cl = Parse::DebianChangelog->init({infile => $file});
    my $entries = $cl->data;

    next unless $entries->[0]->Distribution eq 'UNRELEASED';
    $result{$file} = scalar split(/\n/, $entries->[0]->Changes);

    if ($entries->[1]) {
        $date{$file}      = $entries->[1]->Date;
        $timestamp{$file} = $entries->[1]->Timestamp;
    } else {
        $date{$file}      = $entries->[0]->Date;
        $timestamp{$file} = $entries->[0]->Timestamp;
        $noupload{$file}  = 1;
    }
}

my @result;
if ($bydate) {
    @result = sort { $timestamp{$b} <=> $timestamp{$a}  } keys %result;
} elsif ($bylength) {
    @result = sort { $result{$a} <=> $result{$b} or $a cmp $b } keys %result;
} else {
    die "Assertion: Should be never reached"
}

foreach my $file (@result) {
    next if ($never == 1 and not $noupload{$file});
    next if ($never == 0 and $noupload{$file});

    my $pkg = $file;
    $pkg =~ s:^.*/packages/(.*?)/debian/changelog$:$1:;

    # Properly format dates with single digit day but only one blank before it
    $date{$file} =~ s/^([A-Z][a-z]{2}), (\d) /$1,  $2 /;

    printf "%3i (%s): %-50s%s\n",
        $result{$file},
        $date{$file},
        $pkg,
        $noupload{$file} ? ' (Never uploaded)' : '';
}

=head1 NAME

sort_unreleased_packages.pl - sort pkg-perl's unreleased packages

=head1 SYNOPSIS

=over 12

=item B<sort_unreleased_packages.pl>

[B<--help>]
[B<--date>|B<--time>|B<--length>|B<--unreleased>]
[B<--never>|B<--nonever>]

=back

=head1 DESCRIPTION

This script expects a locally checked out copy of all packages
maintained by the Debian Perl Group (use "mr up" for getting there)
and checks all their changelogs for "UNRELEASED" changes.

It then sorts and outputs the packages by different criterias. It can
also filter on never uploaded packages.

=head1 OPTIONS AND ARGUMENTS

=over 8

=item B<--help>

Print a brief help message and exit.

=item B<--date> or B<--date>

Order output by date and time of last upload (according to the
changelog entries).

=item B<--length> or B<--unreleased>

Order output by the number of lines in the current "UNRELEASED"
changelog entry.

=item B<--never>

Only show never uploaded packages.

=item B<--nonever>

Don't show packages which were never uploaded.

=back

=head1 AUTHOR

Axel Beckert E<lt>abe@debian.orgE<gt>

=head1 COPYRIGHT

Copryright (C) 2014 by Axel Beckert E<lt>abe@debian.orgE<gt>

=head1 LICENSE

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE

Version 2, December 2004

Copyright (C) 2004 Sam Hocevar E<lt>sam@hocevar.netE<gt>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long as
the name is changed.

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE

TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.
