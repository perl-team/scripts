#!/usr/bin/perl

use strict;
use warnings;
use File::Basename;
mkdir("cpan-link-tree");
use File::Listing qw(parse_dir);
my $base = "http://cpan.hexten.net";
my $backbase = "http://backpan.perl.org/";
my @files;
print "Loading ls-lR.gz\n";
open(LISTING, "zcat ls-lR.gz|");
@files = parse_dir(\*LISTING, '+0000');
close LISTING;
print "Cleaning list\n";
@files = map {@{$_}[0]} @{[grep {@{$_}[1] eq "f"} @files]};
@files = grep {/^\.\/authors\/id\/.*\.(?:tar\.gz|tgz)$/} @files;
@files = map {$_ =~ s/^\./$base/; $_;} @files;
print "Generating hash\n";
my %hash = ();
my %basenames = ();
foreach (@files) {
  my $dist = basename($_);
  next if($basenames{$dist});
  $basenames{$dist} = 1;
  $dist =~ s/-[^-]*\.(?:tar\.gz|tgz)$//;
  push @{$hash{$dist}}, $_;
}
print "Loading backpan.txt.gz\n";
open(LISTING, "zcat backpan.txt.gz|");
@files = <LISTING>;
close LISTING;
print "Cleaning list\n";
@files = map {$_ =~ s/^([^ ]*) .*/$1/; chomp; $_;} @files;
@files = grep {/^authors\/id\/.*\.(?:tar\.gz|tgz)$/} @files;
@files = map {$_ =~ s/^/$backbase/; $_;} @files;
print "Adding backpan data to hash\n";
foreach (@files) {
  my $dist = basename($_);
  next if($basenames{$dist});
  $basenames{$dist} = 1;
  $dist =~ s/-[^-]*\.(?:tar\.gz|tgz)$//;
  push @{$hash{$dist}}, $_;
}
print "Writing to tree\n";
foreach(keys %hash) {
  my $str = "";
  foreach(@{$hash{$_}}) {
    $str .= "<a href=\"" . $_ . "\">" . basename($_) . "</a><br />";
  }
  my $f = "cpan-link-tree/" . $_ . ".html";
  my $current = "";
  if(-f $f) {
    open F, $f;
    $current = join '', <F>;
    close F;
  }
  # so that it doesn't re rsync all 80ish megs of data ... my VPS has bandwith limits.
  if($str ne $current) {
    print "Updating: " . basename($f) . "\n";
    open F, ">", $f;
    print F $str;
    close F;
  }
}
