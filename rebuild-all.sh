grep 'Source: ' */debian/control|sed 's/.\+Source: //' | sort | (
    while read p; do
        TMP=`mktemp -d`
        trap "rm -rf $TMP; exit 1" INT QUIT

        echo "=== $p"
        cd $TMP
        apt-get -t unstable -qq -d source $p

        v=`grep '^Version' *.dsc|head -n 1|sed 's/^Version: //'`

        echo -n downloaded $v

        LOG=/tmp/rebuildlogs/$p

        sudo cowbuilder --build --basepath /var/cache/pbuilder/sid+perl5.10 *.dsc > $LOG 2>&1

        echo " built"

        debc /var/cache/pbuilder/result/${p}_${v}_i386.changes >> $LOG

        rm -rf $TMP
    done
)
