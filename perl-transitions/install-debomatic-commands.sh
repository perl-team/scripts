#!/bin/sh

# we try to preserve the original order in file mtime stamps
# hence the sleep calls
#
# split(1) is parallelized so no help there

if [ -n "$DEBOMATIC_SKIP_INSTALL" ]; then
	echo "skipping installation due to \$DEBOMATIC_SKIP_INSTALL" 1>&2
	exit 0
fi

cat "$@" | \
while read command package dist rest; do
    echo "$command $package $dist $rest" > /srv/debomatic/incoming/"$command-$dist-$package".commands
    sleep .01
done
