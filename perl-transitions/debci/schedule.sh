#!/bin/sh
set -e

DRYRUN=false
PRIORITY=3

usage() {
    cat 1>&2 <<EOF
Usage: $0 [ -n ] [ -p <PRIORITY> ] [ -k <KEY> ] <ci-file.json>
       -n: dry run
       -p <PRIORITY>: schedule with PRIORITY (0-10, larger means more important, default $PRIORITY)
       -k <KEY>: API key for ci.debian.net, default taken from the API_KEY environment variable
       <file.json>: see https://ci.debian.net/api/doc
EOF
    exit 1
}

while getopts nk:p: f
do
    case $f in
    n) DRYRUN=true;;
    k) API_KEY="$OPTARG";;
    p) PRIORITY="$OPTARG";;
    *) usage;;
    esac
done
shift `expr $OPTIND - 1`

[ -z "$1" ] && usage
if [ -z "$API_KEY" ]; then
    echo "No API key found, aborting" 1>&2
    usage
fi

if $DRYRUN; then
    CURL="echo curl"
else
    CURL=curl
fi

$CURL -H "Auth-Key: $API_KEY" -F tests=@"$1" -F priority=${PRIORITY} https://ci.debian.net/api/v1/test/unstable/amd64
