#!/bin/sh
set -e

DRYRUN=false
SINCE="1 hour ago"
MATCH=perl

usage() {
    cat 1>&2 <<EOF
Usage: $0 [ -n ] [ -s <SINCE> ] [ -k <KEY> ]
       -n: dry run
       -m <MATCH>: select only results with trigger matching <MATCH> (default "$MATCH")
       -s <SINCE>: how far back to query, in "date -d" format (default "$SINCE")
       -k <KEY>: API key for ci.debian.net, default taken from the API_KEY environment variable
EOF
    exit 1
}

while getopts nk:m:s: f
do
    case $f in
    n) DRYRUN=true;;
    k) API_KEY="$OPTARG";;
    m) MATCH="$OPTARG";;
    s) SINCE="$OPTARG";;
    *) usage;;
    esac
done
shift `expr $OPTIND - 1`

if [ -z "$API_KEY" ]; then
    echo "No API key found, aborting" 1>&2
    usage
fi

if $DRYRUN; then
    CURL="echo curl"
else
    CURL=curl
fi

$CURL -s -H "Auth-Key: $API_KEY"  https://ci.debian.net/api/v1/test\?since=$(date +%s -d "$SINCE") | \
  jq --arg match "$MATCH" '[.results[] | select(.trigger | match($match)) | {(.package): .status}] | add'
