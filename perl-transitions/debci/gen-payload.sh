#!/bin/sh

set -e

TEMPLATE_FILE=templates/perl-next.json
TRIGGER="perl-5.40"

usage() {
    cat 1>&2 <<EOF
Usage: $0 [ -T <TRIGGER> ] [ -t <TEMPLATE> ] [ pkg1 pkg2 ... | - ]
       -t <TEMPLATE>: template file (default "$TEMPLATE_FILE")
       -T <TRIGGER>: use TRIGGER as the trigger name in payload (default "$TRIGGER")
EOF
    exit 1
}

runit() {
	jq --null-input --raw-input --arg trigger "$TRIGGER" --slurpfile tmpl "$TEMPLATE_FILE" '[ { "package": inputs } + $tmpl[0] + { "trigger": $trigger } ]'
}

while getopts t:T: f
do
    case $f in
    t) TEMPLATE_FILE="$OPTARG";;
    T) TRIGGER="$OPTARG";;
    *) usage;;
    esac
done
shift `expr $OPTIND - 1`

NL="
"
if [ -n "$*" ] && [ "$*" != "-" ]; then
    (IFS=$NL; echo "$*") | runit
else
    runit
fi
