#!/bin/bash

set -e
shopt -s nullglob

ARCH=amd64
VER=5.40
NMU_MSG="Rebuild for Perl $VER"
START_ROUND=1

TEMP=$(getopt -o a:r: --long arch:,round: -n 'gen-debomatic-bin.sh' -- "$@")
eval set -- "$TEMP"

while true; do
  case "$1" in
    -a|--arch) ARCH=$2; shift 2 ;;
    -r|--round) START_ROUND=$2; shift 2 ;;
    --) shift; break ;;
    *) echo "Internal error!"; exit 1 ;;
  esac
done

if [ -z "$1" ]; then
    echo "Usage: $0 [ -a ARCH ] [ -r START_ROUND ] perlapi.out"
    exit 1
fi

input_file=$(realpath $1)

if [ ! -r "$input_file" ]; then
    echo "$input_file is not a readable file"
    exit 1
fi

END_ROUND=$(grep -v '^#' $input_file | sort -n -k2 $input_file | tail -1 | cut -f2 -d\  )
ROUNDS=$(seq $START_ROUND $END_ROUND)

for wantround in $ROUNDS; do
    grep -v '^#' $input_file | while read package round srcname_srcvers next_binnmu_number; do
		if [ $round == $wantround ]; then
            echo "binnmu $srcname_srcvers perl-$VER $next_binnmu_number \"$NMU_MSG\" Debian Perl autobuilder <perl@packages.debian.org>"
		fi
	done | sort -u
done
