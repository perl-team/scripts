#!/bin/bash

set -e
shopt -s nullglob

ARCH=amd64
VER=5.22
FULLVER=$VER.0
WORK_PREFIX=~

TEMP=$(getopt -o a:b: --long arch:,basedir: -n 'rebuild-all.sh' -- "$@")
eval set -- "$TEMP"

while true; do
  case "$1" in
    -a|--arch) ARCH=$2; shift 2 ;;
    -b|--basedir) WORK_PREFIX=$2; shift 2 ;;
    --) shift; break ;;
    *) echo "Internal error!"; exit 1 ;;
  esac
done

if [ -z "$1" ]; then
    echo "Usage: $0 [ -a ARCH ] [ -b BASEDIR ] perl.sources"
    exit 1
fi

input_file=$(realpath $1)

if [ ! -r "$input_file" ]; then
    echo "$input_file is not a readable file"
    exit 1
fi

SCHROOT_NAME=perl-$VER-$ARCH-sbuild

mkdir -p $WORK_PREFIX/build $WORK_PREFIX/logs $WORK_PREFIX/build-done

cd $WORK_PREFIX/build

sbuild-update -ud $SCHROOT_NAME
grep -v '^#' $input_file | while read srcname_srcvers; do
	if [ -e "stop-rebuild" ]; then
		echo "Stopping due to flag file 'stop-rebuild'"
		exit 0
	fi
	# This may go on for several archive pushes, and the extra
	# time to install updates into each snapshot adds up
	sbuild-update -ud $SCHROOT_NAME
	sbuild -j 2 -A -q --arch $ARCH -v -c $SCHROOT_NAME -d unstable $srcname_srcvers || true
done
for f in *.build; do
    if [ -L $f ]; then
        rm -f $f
    else
        mv $f $WORK_PREFIX/logs
    fi
done
for f in *; do
    mv $f $WORK_PREFIX/build-done
done
