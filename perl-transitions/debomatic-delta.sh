#!/bin/bash

set -e

export APT_CONFIG=apt.conf

APT_REPO_URL=${APT_REPO_URL:-http://deb.debian.org/debian}

# XXX add locking

if [ "$1" = "" ]; then
    echo "Usage: $0 <file-to-diff>" 1>&2
    exit 1
fi

target="$1"

base=$(basename "$target" .commands)

stampfile="$base".delta-checksum
deltabase="$base".fordelta
commandsfile="$base"-$(date --rfc-3339=seconds|sed -e 's/ /_/').commands

make -s APT_REPO_URL="${APT_REPO_URL}" apt-update >/dev/null

if [ -r $stampfile ] && sha1sum --check --status $stampfile; then
    exit 0
fi

if [ ! -e $deltabase ]; then
    touch $deltabase
fi

cp $deltabase ${deltabase}.bak
if ! make -s "$target"; then
    echo "cannot make \"$target\", aborting" 1>&2
    exit 1
fi

if ! [ -f "$target" ]; then
    echo "File $target missing even after re-make, aborting" 1>&2
    exit 1
fi

sort "$target" > "$deltabase"
comm -13 ${deltabase}.bak ${deltabase} > $commandsfile

if [ -s "$commandsfile" ]; then
    echo "Scheduling: "
    cat $commandsfile
    ./install-debomatic-commands.sh $commandsfile
else
    rm $commandsfile
fi

packages_file=$(./print-packages-file)
sha1sum $packages_file > $stampfile
