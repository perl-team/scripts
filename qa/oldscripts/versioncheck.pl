#!/usr/bin/perl -w
# Copyright gregor herrmann <gregor+debian@comodo.priv.at>, 2007
# Copyright Damyan Ivanov <dmn@debian.org>, 2007
# Released under the terms of the GNU GPL 2

### TODO ###
#
# Try harder to use 02packages.details.gz for authoritative CPAN
#  version source, regardless of whether debian/watch uses by-module URL
#  or by-author one
#
# Use AptPkg::Version for
#  - version comparison
#  - stripping debian revision off from a version

our $THIS_REVISION = '$Id$';

BEGIN {
    my $self_dir = $0;
    $self_dir =~ s{/[^/]+$}{};
    unshift @INC, $self_dir;
};

use strict;
use Common;
use LWP::Simple ();
use Compress::Zlib ();
use HTML::TableExtract;
use SVN::Client;
use SVN::Core;
use IO::Scalar;
use Parse::DebianChangelog;
use Getopt::Long;

our $opt_debug = 0;

GetOptions(
    'debug!'    => \$opt_debug,
);

sub debugmsg(@)
{
    warn @_ if $opt_debug;
};


# Get some information globally

use Storable();
use LWP::UserAgent;

debugmsg( "CPAN mirror is $CPAN_MIRROR\n" );
debugmsg( "HOME=$ENV{HOME}\n" );

sub from_cache($$$)
{
    my( $ref, $name, $max_age) = @_;

    my $dir = $ENV{HOME}.'/.dpg/versioncheck';

    return undef unless -f "$dir/$name" and -M(_) <= $max_age/24;

    my $data = Storable::retrieve("$dir/$name");
    return undef unless $data;

    debugmsg("$name loaded from cache (".scalar(keys(%$data)).")\n");

    %$ref = %$data;
    return 1;
}

sub to_cache($$)
{
    my( $ref, $name) = @_;

    my $home = $ENV{HOME};

    -d "$home/.dpg" or mkdir("$home/.dpg") or die $!;
    -d "$home/.dpg/versioncheck" or mkdir("$home/.dpg/versioncheck") or die $!;

    Storable::store($ref, "$home/.dpg/versioncheck/$name");
}

sub scan_packages($$)
{
    my( $suite, $hash ) = @_;
    foreach my $section ( qw( main contrib non-free ) )
    {
        # TODO This is somewhat brute-force, reading the whole sources into
        # memory, then de-compressing them also in memory.
        # Should be made incremental using reasonable-sized buffer
        my $url = "$MIRROR/debian/dists/$suite/$section/source/Sources.gz";
        my $sources_gz = LWP::Simple::get($url);
        $sources_gz or die "Can't download $url";
        my $sources = Compress::Zlib::memGunzip(\$sources_gz);
        my $src_io = IO::Scalar->new(\$sources);

        my $pkg;
        while( <$src_io> )
        {
            chomp;
            if( s/^Package: // )
            {
                $pkg = $_;
                next;
            }

            if( s/^Version: // )
            {
                $hash->{$pkg} = $_;
            }
        }
    }

    debugmsg(
        sprintf(
            "Information about %d %s packages loaded\n",
            scalar(keys(%$hash)),
            $suite,
        ),
    );
    to_cache($hash, $suite);
}

my %packages;   # contains {package => version} pairs
scan_packages(
    'unstable', \%packages,
) unless from_cache(\%packages, 'unstable', 6);

my %experimental;   # contains {package => version} pairs
scan_packages(
    'experimental', \%experimental,
) unless from_cache(\%experimental, 'experimental', 6);

my %stable;   # contains {package => version} pairs
scan_packages(
    'stable', \%stable,
) unless from_cache(\%stable, 'stable', 168);   # 1 week

my %oldstable;   # contains {package => version} pairs
scan_packages(
    'oldstable', \%oldstable,
) unless from_cache(\%oldstable, 'oldstable', 168); # 1 week


my %incoming;   # contains {package => version} pairs
do {
    my $incoming = LWP::Simple::get('http://incoming.debian.org')
        or die "Unable to retreive http://incoming.debian.org";
    my $inc_io = IO::Scalar->new(\$incoming);
    while( <$inc_io> )
    {
        chomp;
        next unless /a href="([^_]+)_(.+)\.dsc"/;

        $incoming{$1} = $2;
    }
};
debugmsg( sprintf("Information about %d incoming packages loaded\n", scalar(keys(%incoming))) );

my %new;    # contains {package => version} pairs
do {
    my  $new = LWP::Simple::get('http://ftp-master.debian.org/new.html');
    my $te = HTML::TableExtract->new(
        headers=> [
            qw(Package Version Arch Distribution Age Maintainer Closes)
        ],
    );
    $te->parse($new);
    foreach my $table( $te->tables )
    {
        foreach my $row( $table->rows )
        {
            next unless $row->[2] =~ /source/;

            my @versions = split(/\n/, $row->[1]);
            s/<br>// foreach @versions;

            $new{$row->[0]} = $versions[-1];# use the last uploaded version
        }
    }
};
debugmsg( sprintf("Information about %d NEW packages loaded\n", scalar(keys(%new))) );

my %cpan_authors;
my %cpan_modules;
do {
    open(TMP, '+>', undef) or die "Unable to open anonymous temporary file";
    my $old = select(TMP);
    my $lslr = LWP::Simple::getprint("$CPAN_MIRROR/ls-lR.gz");
    select($old);
    seek(TMP, 0, 0);
    my $gz = Compress::Zlib::gzopen(\*TMP, 'rb') or die $Compress::Zlib::gzerrno;

    my $storage;
    my ($section, $path);
    while( $gz->gzreadline($_) )
    {
        chomp;
        next unless $_;

        if( m{^\./authors/id/(.+):} )
        {
            $storage = $cpan_authors{$1} ||= [];
        }
        elsif( m{^\./modules/by-module/(.+):} )
        {
            $storage = $cpan_modules{$1} ||= [];
        }
        elsif( m{\..*:} )
        {
            undef($storage);
        }
        else
        {
            next unless $storage;

            my(
                $perm, $ln, $o, $g, $size, $month, $day, $time, $what, $where,
            ) =  split(/\s+/);

            next unless $what and $what =~ /(?:tar\.gz|\.tgz|\.zip|\.tar\.bz2|\.tbz)$/;

            push @$storage, $what;
        }
    }
    close(TMP);

    to_cache(\%cpan_modules, 'cpan_modules');
    to_cache(\%cpan_authors, 'cpan_authors');
} unless from_cache(\%cpan_authors, 'cpan_authors', 12)
    and from_cache(\%cpan_modules, 'cpan_modules', 12);


# RETURNS
#  1 if first version is bigger
#  0 if both versions are equal
# -1 if second version is bigger
sub cmp_ver($$)
{
    my($a,$b) = @_;

    while( $a and $b )
    {
        $a =~ s/^(\w*)//; my $a_w = $1||'';
        $b =~ s/^(\w*)//; my $b_w = $1||'';

        my $r = $a_w cmp $b_w;

        return $r if $r;

        $a =~ s/^(\d*)//; my $a_d = (defined($1) and $1 ne '') ? $1 : -1;
        $b =~ s/^(\d*)//; my $b_d = (defined($1) and $1 ne '') ? $1 : -1;

        $r = $a_d <=> $b_d;

        return $r if $r;

        $a =~ s/^(\D*)//; my $a_nd = $1||'';
        $b =~ s/^(\D*)//; my $b_nd = $1||'';

        $r = $a_nd cmp $b_nd;

        return $r if $r;
    }
    return 1 if $a;
    return -1 if $b;
    return 0;
}

sub unmangle( $ $ )
{
    my( $ver, $mangles ) = @_;

    return $ver unless $mangles;

    my @vms = map( split(/;/, $_), @$mangles );

    foreach my $vm( @vms )
    {
        eval "\$ver =~ $vm";
        die "<<\$_ =~ $vm>> $@" if $@;
        debugmsg("     mangled: $ver\n");
    }

    return $ver;
}

# RETURNS undef if all watch files point to CPAN
sub latest_upstream_from_watch($)
{
    my ($watch) = @_;

    my @vers;

    foreach(@$watch)
    {
        my( $wline, $opts ) = @$_;

        $wline =~ m{^(http://\S+)/};
        my $url = $1;
        $url =~ s{^http://sf.net/}{http://sf.net.projects/};

        $wline =~ s{^http://sf\.net/(\S+)}{http://qa.debian.org/watch/sf.php/$1};
        if( $wline =~ m{
                ^((?:http|ftp)://\S*?)  # http://server/some/path - captured
                                        #  non-greedy to not eat up the pattern
                (?:/\s*|\s+)            # delimiter - '/' for ver3 or space for ver2
                ([^\s/]+)               # the search pattern - no spaces, no slashes - captured
                (?:
                    (?!.*\()            # followed by non-(search pattern)
                    |
                    \s*$                # or EOL
                )
            }ix )
        {
            my( $dir, $filter ) = ($1, $2);
            debugmsg( "   uscan $dir $filter\n" );
            $url ||= $dir;
            my $page = LWP::Simple::get($dir) or return "Unable to get $dir (".__LINE__.")";
            my $page_io = IO::Scalar->new(\$page);
            while( <$page_io> )
            {
                warn $_ if 1;

                if( $dir =~ /^http/ )
                {
                    while( s/<a [^>]*href="([^"]+)"[^>]*>//i )
                    {
                        my $href = $1;
                        push @vers, [
                            unmangle( $1, $opts->{uversionmangle} ),
                            $url,
                        ] if $href =~ $filter;
                    }
                }
                else
                {
                    while( s/(?:^|\s+)$filter(?:\s+|$)// )
                    {
                        push @vers, [
                            unmangle( $1, $opts->{uversionmangle} ),
                            $url,
                        ];
                    }
                }
            }
        }
        else
        {
            return "bad watch URL $wline";
        }
    }

    @vers = sort { cmp_ver($a->[0],$b->[0]) } @vers;

    my $ver = $vers[-1] || '';
    my $url;

    ($ver, $url) = $ver ? @$ver : (undef, undef);

    return wantarray ? ($ver, $url) : $ver;
}

# returns array of [ver, path]
sub cpan_versions($$$)
{
    my($where, $wline, $opts) = @_;

    $wline =~ m{
                ^(\S*?)                 # some/path - captured
                                        #  non-greedy to not eat up the pattern
                (?:/\s*|\s+)            # delimiter - '/' for ver3 or space for ver2
                ([^\s/]+)               # the search pattern - no spaces, no slashes - captured
                (?!.*\()                # not followed by search pattern
            }ix;
    my( $key, $filter) = ($1, $2);
    debugmsg( sprintf( "   module search %s %s\n", $key, $filter ) );

    my $list = $where->{$key};
    unless($list)
    {
        debugmsg("directory $key not found (from $wline) [".__LINE__."]\n");
        return();
    }

    my @vers;
    foreach(@$list)
    {
        if( $_ =~ $filter )
        {
            debugmsg("     looking at $_\n") if 1;
            my $ver = unmangle( $1, $opts->{uversionmangle} );
            push @vers, [$ver, $key];
        }
    }

    return @vers;
}

# returns (version, URL)
sub latest_upstream_from_cpan($)
{
    my ($watch) = @_;

    my @cpan = grep( $_->[0] =~ m{(?:^|\s)(?:http|ftp)://\S*cpan}i, @$watch );

    return undef unless @cpan;

    my @vers;

    foreach(@cpan)
    {
        my( $wline, $opts ) = @$_;
        if( $wline =~ s{^(?:http|ftp)://\S*cpan\S*/modules/by-module/}{}i )
        {
            # lookup by module
            push @vers, map(
                [ $_->[0], "http://www.cpan.org/modules/by-module/".$_->[1] ],
                cpan_versions(\%cpan_modules, $wline, $opts),
            );
        }
        elsif( $wline =~ s{^(?:http|ftp)://\S*cpan\S*/authors/(?:by-)?id/}{}i
                or
            $wline =~ s{^(?:http|ftp)://\S*cpan\S*/(?:by-)?authors/id/}{}i
        )
        {
            # lookup by author
            push @vers, map(
                [ $_->[0], "http://www.cpan.org/authors/id/".$_->[1] ],
                cpan_versions(\%cpan_authors, $wline, $opts),
            );
        }
        else
        {
            debugmsg( sprintf( "    can't determine type of search for %s\n", $wline ) );
            return undef;
        }
    }

    @vers = sort { cmp_ver($a->[0],$b->[0]) } @vers;

    my $ver = $vers[-1];
    my $url;
    if( $ver )
    {
        ($ver, $url) = @$ver;
    }
    else
    {
        undef($ver); undef($url);
    }

    return wantarray ? ($ver, $url) : $ver;
}

sub unmangle_debian_version($$)
{
    my($ver, $watch) = @_;

    foreach( @$watch )
    {
        my $dvm = $_->[1]->{dversionmangle} if $_->[1];
        $dvm ||= [];

        do {
            eval "\$ver =~ $_";
            die "\$ver =~ $dvm  -> $@" if $@;
        } foreach @$dvm;
    }

    return $ver;
}


print <<_EOF;
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>pkg-perl package versions</title>
	<style type="text/css">
		body {
			background: white;
			color: black;
		}
		table {
			border: 1px solid black;
			border-collapse: collapse;
                        empty-cells: show;
		}
		td, th {
			border: 1px solid black;
		}
		.upload {
			background: lightsalmon;
		}
		.upgrade {
			background: lightblue;
		}
	</style>
</head>
<body>
<table>
<tr><th>Legend</th></tr>
<tr><td class="upload">Needs uploading</td></tr>
<tr><td class="upgrade">Needs upgrade from upstream</td></tr>
</table>

<br>

<table>
<tr>
	<th>Package</th>
	<th>Repository</th>
	<th>Archive</th>
	<th>upstream</th>
</tr>
_EOF

my $total = 0;
my $total_shown = 0;
my $svn = SVN::Client->new();

sub check_package($)
{
    my( $dir ) = @_;

    debugmsg( "Examining $dir\n" );

    my $pkg = "";
    my $changelog = "";

    my $in_svn = 'Unknown SVN version';
    my $svn_changer = "";
    my $svn_date = "";
    my $svn_error;
    my $svn = SVN::Client->new();
    {
        my $changelog_fh = IO::Scalar->new( \$changelog );
        local $SVN::Error::handler = undef;
        ($svn_error) = $svn->cat(
            $changelog_fh,
            "$SVN_REPO/trunk/$dir/debian/changelog",
            'HEAD',
        );
    }
    if(SVN::Error::is_error($svn_error))
    {
        if($svn_error->apr_err() == $SVN::Error::FS_NOT_FOUND)
        {
            $in_svn = 'Missing debian/changelog';
            $svn_error->clear();
        }
        else
        {
            SVN::Error::croak_on_error($svn_error);
        }
    }
    my @cl;
    if($changelog) {
        @cl = Parse::DebianChangelog->init({instring=>$changelog})->data;
    }
    foreach( @cl )
    {
        next unless $_->Distribution eq 'unstable';
        next if $_->Changes =~ /NOT RELEASED/;

        $in_svn = $_->Version;
        $svn_changer = $_->Maintainer;
        $svn_date = $_->Date;
        $pkg = $_->Source;
        last;
    }

    my $in_archive = $packages{$pkg} || '';
    debugmsg( sprintf(" - Archive has %s\n", $in_archive||'none') );

    my $in_experimental = $experimental{$pkg};
    debugmsg( sprintf( " - experimental has %s\n", $in_experimental||'none' ) );

    my $in_stable = $stable{$pkg};
    debugmsg( sprintf( " - stable has %s\n", $in_stable||'none' ) );

    my $in_oldstable = $oldstable{$pkg};
    debugmsg( sprintf( " - oldstable has %s\n", $in_oldstable||'none' ) );


    my $upstream = '';
    my $upstream_is_cpan;
    my $in_cpan = '';
    my $upstream_url;
    my @watch;
    my $watch;
    {
        my $watch_io = IO::Scalar->new(\$watch);
        local $SVN::Error::handler = undef;
        ($svn_error) = $svn->cat(
            $watch_io,
            "$SVN_REPO/trunk/$dir/debian/watch",
            'HEAD',
        );
        $watch_io->close();
    }
    if(SVN::Error::is_error($svn_error))
    {
        if($svn_error->apr_err() == $SVN::Error::FS_NOT_FOUND)
        {
            $upstream = (
                ( $in_svn =~ /-.+$/ )
                ? 'Missing debian/watch'
                : $in_svn # native package
            );
            $svn_error->clear();
            $watch = "";
        }
        else
        {
            SVN::Error::croak_on_error($svn_error);
        }
    }

    $watch =~ s/\\\n//gs;
    my @watch_lines = split(/\n/, $watch) if $watch;

    @watch_lines = grep( (!/^#/ and !/^version=/ and !/^\s*$/), @watch_lines );

    foreach(@watch_lines)
    {
        debugmsg( "   watch line $_\n" ) if 0;
        # opts either contain no spaces, or is enclosed in double-quotes
        my $opts = $1 if s!^\s*opts="([^"]*)"\s+!! or s!^\s*opts=(\S*)\s+!!;
        debugmsg( "     watch options = $opts\n" ) if $opts;
        # several options are separated by comma and commas are not allowed within
        my @opts = split(/\s*,\s*/, $opts) if $opts;
        my %opts;
        foreach(@opts)
        {
            next if /^(?:active|passive|pasv)$/;

            /([^=]+)=(.*)/;
            debugmsg( "      watch option $1 = $2\n" );
            if( $1 eq 'versionmangle' )
            {
                push @{ $opts{uversionmangle} }, $2;
                push @{ $opts{dversionmangle} }, $2;
            }
            else
            {
                push @{ $opts{$1} }, $2;
            }
        }
        s!^http://www.cpan.org/!$CPAN_MIRROR/!;
        s!^ftp://www.cpan.org/!$CPAN_MIRROR/!;
        s!^http://backpan.perl.org/authors/!$CPAN_MIRROR/authors/!;
        s!^http://mirrors.kernel.org/cpan/!$CPAN_MIRROR/!;
        s!^ftp://mirrors.kernel.org/cpan/!$CPAN_MIRROR/!;

        push @watch, [ $_, \%opts ];
    }

    my $up_svn = $in_svn;
    $up_svn =~ s/^(?:\d+:)?(.+?)(?:-[^-]+)?$/$1/ if $up_svn;
    $up_svn = unmangle_debian_version($up_svn, \@watch) if @watch;
    debugmsg(
        sprintf(
            " - SVN has %s (upstream version=%s)\n",
            $in_svn||'none',
            $up_svn||'none',
        )
    );

    if( @watch )
    {
        ($in_cpan,  $upstream_url) = latest_upstream_from_cpan(\@watch);
        debugmsg( sprintf( " - CPAN has %s (%s)\n", $in_cpan||'none', $upstream_url||'no url' ) );
        if( $in_cpan )
        {
            $upstream_is_cpan = 1;
            $upstream = $in_cpan;
        }
        else
        {
            ($upstream, $upstream_url) = latest_upstream_from_watch(\@watch);
        }
        debugmsg( sprintf( " - upstream has %s (%s)\n", $upstream||'none', $upstream_url||'no url' ) );
    }
    else
    {
        $upstream ||= (
            ( $in_svn =~ /-.+$/ )
            ? qq(Invalid <a href="http://svn.debian.org/wsvn/pkg-perl/trunk/$dir/debian/watch?op=file&amp;rev=0&amp;sc=0">debian/watch</a>)
            : $in_svn # native package
        );
    }


    my $in_incoming = $incoming{$pkg}||'';
    debugmsg( sprintf( " - incoming has %s\n", $in_incoming||'none' ) );
    my $in_new = $new{$pkg}||'';
    debugmsg( sprintf( " - NEW has %s\n", $in_new||'none' ) );



    if( $up_svn ne $upstream
            or
        $in_svn ne $in_archive
            and
        $in_svn ne $in_incoming
            and
        $in_svn ne $in_new
    )
    {
        print "<tr>\n";
        print "<td>".(
            ($in_archive)
            ? qq(<a href="http://packages.qa.debian.org/$pkg">$pkg</a>)
            : qq($pkg)
        )."</td>\n";

        my $in_svn_text = qq(<a href="http://svn.debian.org/wsvn/pkg-perl/trunk/$dir/debian/changelog?op=file&amp;rev=0&amp;sc=0" title="$svn_changer\n$svn_date">$in_svn</a>);
        print "<td".(
            ($in_svn ne $in_archive)
            ? ' class="upload"'
            : ''
        ).">$in_svn_text</td>\n";

        my $archive_text = join(
            "\n",
            $in_archive||(),
            (
                ($in_incoming)
                ? "Incoming:&nbsp;$in_incoming"
                : ()
            ),
            (
                ($in_new)
                ? "NEW:&nbsp;$in_new"
                : ()
            ),
            (
                ($in_experimental)
                ? "experimental:&nbsp;$in_experimental"
                : ()
            ),
            (
                ($in_stable and not $in_archive and not $in_experimental)
                ? "stable:&nbsp;$in_stable"
                : ()
            ),
            (
                ($in_oldstable and not $in_stable and not $in_archive and not $in_experimental)
                ? "oldstable:&nbsp;$in_oldstable"
                : ()
            ),
        );

        $archive_text = qq(<a href="http://packages.qa.debian.org/$pkg">$archive_text</a> [<a style="font-size:smaller" href="http://bugs.debian.org/src:$pkg">BTS</a>]) if $in_archive or $in_experimental or $in_stable or $in_oldstable;

        print "<td>$archive_text</td>\n";

        my $upstream_text = (
            $upstream_is_cpan
            ? "CPAN:&nbsp;$in_cpan"
            : $upstream
        );
        $upstream_text = qq(<a href="$upstream_url">$upstream_text</a>) if $upstream_url;

        print(
            ($up_svn ne $upstream)
            ? qq(<td class="upgrade">$upstream_text</td>\n)
            : "<td></td>\n"
        );
        print "</tr>\n";

        return 1;
    }

    return 0;
}

my @pkgs_to_check;
if( @ARGV )
{
    @pkgs_to_check = @ARGV;
}
else
{
# loop over packages
    my $svn_packages = $svn->ls("$SVN_REPO/trunk", 'HEAD', 0);

    debugmsg(
        sprintf(
            "%d entries in trunk\n",
            scalar(keys(%$svn_packages)),
        ),
    );
    @pkgs_to_check = sort(keys %$svn_packages);
}
foreach my $pkg( @pkgs_to_check )
{
    $total++;

    $total_shown++ if check_package($pkg);
}

my $date = gmtime;
print <<_EOF;
<tr><td colspan=\"4\"><b>TOTAL: $total_shown/$total</b></td></tr>
</table>
<hr>
$date UTC<br>
<i>$THIS_REVISION</i>
</body>
_EOF


exit 0

# vim: et:sts=4:ai:sw=4
