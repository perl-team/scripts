# $Id$
package Common;
use strict;
use Sys::Hostname;
use base 'Exporter';

our @EXPORT = qw(
    $SVN_REPO
    $MIRROR
    $CPAN_MIRROR
);

our $SVN_REPO = "svn://svn.debian.org/svn/pkg-perl";
our $MIRROR = "MIRROR=ftp://ftp.debian.org";
our $CPAN_MIRROR = "ftp://cpan.org/pub/CPAN";

# special hosts
for( hostname )
{
    # alioth
    /alioth/ && do {
        $SVN_REPO = "file:///svn/pkg-perl";
        $MIRROR = "ftp://ftp.nl.debian.org";
        $CPAN_MIRROR = "ftp://cpan.wanadoo.nl/pub/CPAN";
        last;
    };

    # Gregor
    /belanna|nerys/ && do {
        $MIRROR = "ftp://ftp.at.debian.org";
        $CPAN_MIRROR = "ftp://gd.tuwien.ac.at/pub/CPAN";
        last;
    };

    # dam
    /pc1/ && do {
        $MIRROR = "http://proxy:9999";
        $CPAN_MIRROR = "ftp://ftp.uni-sofia.bg/cpan";
        last;
    };
    /beetle/ && do {
        $MIRROR = "http://localhost:9999";
        $CPAN_MIRROR = "ftp://ftp.uni-sofia.bg/cpan";
        last;
    };

    # Tincho
    /abraxas/ && do {
        $MIRROR = "file:///media/IOMega/mirror/";
        $CPAN_MIRROR = "ftp://cpan.ip.pt/pub/cpan/";
        last;
    };

    die "Unknown host $_";
}

# This mirror is near alioth. From #alioth:
# <ard> ard@c32791:~$ sudo  /usr/sbin/traceroute -A cpan.wanadoo.nl|grep AS1200
# <ard> traceroute to ftp.wanadoo.nl (194.134.17.10), 64 hops max, 40 byte packets
# <ard>  5  ams-ix.euro.net (195.69.144.70) [AS1200]  1 ms  1 ms  1 ms
# <ard> jups
# <ard> 10G going to as1200
# <ard> As long as it passes as1200 it's ok... Everything else is $$ :-(
# CPAN=ftp://cpan.wanadoo.nl/pub/CPAN

use CPAN;
my $home = $ENV{HOME};
$CPAN::Config = {
  'build_cache' => q[10],
  'build_dir' => "$home/.cpan/build",
  'cache_metadata' => q[1],
  'cpan_home' => "$home/.cpan",
  'cpan_version_check' => q[0],
  'dontload_hash' => {  },
  'ftp' => q[],
  'ftp_proxy' => q[],
  'getcwd' => q[cwd],
  'gpg' => q[/usr/bin/gpg],
  'gzip' => q[/bin/gzip],
  'histfile' => "/dev/null",
  'histsize' => q[100],
  'http_proxy' => q[],
  'inactivity_timeout' => q[0],
  'index_expire' => q[1],
  'inhibit_startup_message' => q[1],
  'keep_source_where' => "$home/.cpan/sources",
  'lynx' => q[/usr/bin/lynx],
  'make' => q[/usr/bin/make],
  'make_arg' => q[],
  'make_install_arg' => q[],
  'makepl_arg' => q[INSTALLDIRS=site],
  'ncftp' => q[],
  'ncftpget' => q[],
  'no_proxy' => q[],
  'pager' => q[/usr/bin/less],
  'prerequisites_policy' => q[ignore],
  'scan_cache' => q[never],
  'shell' => q[/bin/bash],
  'tar' => q[/bin/tar],
  'term_is_latin' => q[0],
  'unzip' => q[],
  'urllist' => [ $CPAN_MIRROR ],
  'wget' => q[/usr/bin/wget],
};

1;
