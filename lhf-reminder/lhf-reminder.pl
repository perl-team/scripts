#!/usr/bin/perl
#
# Copyright 2015 Dominic Hargreaves <dom@earth.li>
#
# This program is licensed under the terms of the GNU General Public License
# (GPL), version 2 or later, as published by the Free Software Foundation.

use strict;
use warnings;

use Template;
use MIME::Lite;
use Readonly;
use DateTime;
use LWP::UserAgent;

#### Config starts here
Readonly my $info_url => 'https://wiki.debian.org/Teams/DebianPerlGroup/LHF';
Readonly my $agenda_url => 'https://wiki.debian.org/Teams/DebianPerlGroup/LHF/Agenda';
Readonly my $dom => 21;
Readonly my @days_to_run => (6, 1);

Readonly my $me => 'Dominic Hargreaves <dom@earth.li>';
Readonly my $dest => 'Debian Perl List <debian-perl@lists.debian.org>';
#### Config ends here

my $agenda_url_raw = $agenda_url . '?action=raw';
my $today = DateTime->today();
my $time = ($today->month % 2) ? '17:00' : '19:00';
# XXX better not try and run in the previous month
my $next_date = DateTime->new(
	year => $today->year,
	month => $today->month,
	day => $dom
);

my $days_until;
foreach my $day_to_run (@days_to_run) {
    my $next_date_clone = $next_date->clone;
    if ($next_date_clone->subtract(days => $day_to_run) == $today) {
        $days_until = $day_to_run;
        last;
    }
}

die "$0 refusing to run:\nReview the settings in the script and then touch .lhf-reminder-config-ok\n" unless -e '.lhf-reminder-config-ok';

exit 0 unless $days_until;

my $ua = LWP::UserAgent->new(
    agent => 'lhf-reminder', # Needed to work around silly moin user-agent ACLs
);
my $response = $ua->get($agenda_url_raw);

die "Couldn't fetch $agenda_url_raw: " . $response->status_line
    unless $response->is_success;

my $date_reminder_str = $next_date->strftime("%Y-%m-%d") . ", $time UTC";

my %vars = (
    info_url => $info_url,
    agenda => $response->content,
    date => $date_reminder_str,
    dow => $next_date->day_name,
    agenda_url => $agenda_url,
    days_until => $days_until,
);

my $tt = Template->new;
my $msg_content;
$tt->process('lhf-reminder.tt', \%vars, \$msg_content);

$date_reminder_str .= ' (tomorrow)' if $days_until == 1;

my $msg = MIME::Lite->new(
    From => $me,
    To   => $dest,
    Subject => 'Low-hanging fruits reminder for ' . $date_reminder_str,
    Data => $msg_content,
);

$msg->send;
